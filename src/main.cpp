// standard libraries
# include <iostream>
# include <cstdio>
# include <vector>
# include <string>
# include <random>
# include <iterator>
# include <stdexcept>
# include <cstdlib>
# include <time.h> 
# include <fstream> 
# include <map>
# include <sys/types.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sstream>

#include <sys/wait.h>  /*****  For waitpid.                   *****/
#include <setjmp.h>    /*****  For sigsetjmp and siglongjmp.  *****/
#include <signal.h>
#include <unistd.h>

# include "parameter_handler.h"
# include "ticTacToe.h"

std::default_random_engine generator;


// Main
int main(int argc, char *argv[])
{   
    std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);

    //std::string t_val;
    bool parallelization = std::stoi(PARAMETER::get_value(params, "-parallelization")) > 0;
    bool pruning = std::stoi(PARAMETER::get_value(params, "-pruning")) > 0;
    bool static_scheduling = std::stoi(PARAMETER::get_value(params, "-static_scheduling")) > 0;

    /*
    if (argc < 3)
    {
        std::cout << "Error en el numero de parametros" << std::endl;
        exit(-1);
    }
    */

    //std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);
    TTT4 g1;
    // true with pruning, short solution time
    // false without prining a largest solution time
    //bool static_scheduling = false;
    //bool pruning = true;
    //bool parallelization = true;
    g1.play(parallelization, pruning, static_scheduling);

    return 0;
}

