/*
 * DifferentialEvolution.h
 *
 */

// semaphore
#ifndef SEMAPHORE_H
#define SEMAPHORE_H


//// Standard includes. /////////////////////////////////////////////////////

# include <iostream>
# include <cstdio>
# include <vector>
# include <string>
# include <random>
# include <iterator>
# include <stdexcept>
# include <cstdlib>
# include <time.h> 
# include <fstream> 
# include <map>
# include <sys/types.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/sem.h>
# include <sys/ipc.h>
# include <sys/shm.h>

# include <sys/wait.h>  /*****  For waitpid.                   *****/
# include <setjmp.h>    /*****  For sigsetjmp and siglongjmp.  *****/
# include <signal.h>
# include <unistd.h>

 //// Global Vars	 ////////////////////////////////////////////////

//// Definitions/namespaces. ////////////////////////////////////////////////


key_t get_key( int val );
int open_semaphore( key_t keyval);
void init_semaphore( int sid, int initval);
void delete_semaphore(int sid);
void delete_semaphore(std::vector<int> sids);
void down_semaphore(int sid);
void wait_semaphore(int sid);
void up_semaphore(int sid);


int create_shared_memory(int size_of_shmem, std::default_random_engine generator);
char * attach_shared_memory(int shmid);
void delete_shared_memory(std::vector<int> sids);
# endif
