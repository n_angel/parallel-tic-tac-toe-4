/*
 * DifferentialEvolution.h
 *
 */


#ifndef TTT4_H
#define TTT4_H


//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <map>
# include <cassert>
# include <random>
# include <iostream>
# include <fstream>
# include <time.h> 
# include <set>
# include <string.h>
# include <stdlib.h>
# include <algorithm>
# include <sys/types.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/sem.h>
# include <sys/types.h>
# include <unistd.h>

# include <sys/wait.h>  /*****  For waitpid.                   *****/
# include <setjmp.h>    /*****  For sigsetjmp and siglongjmp.  *****/
# include <signal.h>
# include <unistd.h>

# include "semaphore.h"

typedef struct sttgy
{
	int human_winner_cells;
	int computer_winner_cells;
	std::set<int> strategy;
}W_STRATEGY;


typedef struct move
{
	int points;
	int action;
}MOVE;
 //// Global Vars	 ////////////////////////////////////////////////
//extern std::map<std::string, int>  func_code;


MOVE parallel_section_static(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies, bool pruning);
MOVE parallel_section_dynamic(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies, bool pruning);
MOVE max_value(int points, std::vector< int > free_position, W_STRATEGY *strategies, int t_strategies, int best_points, bool pruning);
MOVE min_value(int points, std::vector< int > free_position, W_STRATEGY *strategies, int t_strategies, int best_points, bool pruning);
MOVE lineal_section(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies);

//// Definitions/namespaces. ////////////////////////////////////////////////
class TTT4
{
	private:
		//std::ofstream file_output;
		std::vector< int > free_position;
		//std::vector< std::vector<char> > table;
		std::string table;
		std::vector< W_STRATEGY > strategies;
		//std::vector< W_STRATEGY > human_strategies;

		std::default_random_engine generator;

		void init();
		void human_move(  );
		void computer_move(int pos);
		void increase_winner_strategy(int cell, std::string strategy);
		//void computer_move(int pos);
		//void erase_strategy(int cell);
		//void increase_winner_strategy(int cell, std::string strategy);

		MOVE minimax(int points, 
					std::vector< int > free_position, 
					std::vector< W_STRATEGY > strategies, 
					int t_strategies, 
					bool pruning,
					bool static_scheduling,
					bool parallelization);
		// return the point for a strategy

	public:
		TTT4();
		void print_table();
		//void print_strategy(  );
		void print_strategy(  );
		void play(bool parallelization, bool pruning, bool static_scheduling);

		//MOVE minimax(int points, std::vector< int > free_position, std::vector< W_STRATEGY > strategies);
		//MOVE max_value(int points, std::vector< int > free_position, std::vector< W_STRATEGY > strategies);
		//MOVE min_value(int points, std::vector< int > free_position, std::vector< W_STRATEGY > strategies);
};

 /*
class OUTPUT
{
	private:
		std::ofstream file_output;

	public:
		OUTPUT(std::string filename);
		void write(std::string line);
		void close(  );
};
*/

# endif
