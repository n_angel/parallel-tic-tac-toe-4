/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "ticTacToe.h"

//// Standard includes. /////////////////////////////////////////////////////


//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////
TTT4::TTT4()
{
	this->init();
}

void TTT4::init()
{
	static const int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	this->free_position = std::vector<int>(arr, arr + sizeof(arr) / sizeof(arr[0]) );
	this->table = "                ";

	this->generator = std::default_random_engine();
	this->generator.seed(time(NULL));
	W_STRATEGY stgy;
	stgy.human_winner_cells = 0;
	stgy.computer_winner_cells = 0;

	int info[] = {0, 5, 10, 15};
  	stgy.strategy = std::set<int>(info,info+4); 
  	this->strategies.push_back(stgy);

	stgy.strategy = {0, 4, 8, 12};
  	this->strategies.push_back(stgy);

  	stgy.strategy = {0, 1, 2, 3};
  	this->strategies.push_back(stgy);

  	stgy.strategy = {1, 5, 9, 13}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {1, 6, 11}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {2, 6, 10, 14}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {2, 5, 8}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {3, 6, 9, 12}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {3, 7, 11, 15}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {4, 5, 6, 7}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {4, 9, 14}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {7, 10, 13}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {8, 9, 10, 11}; 
  	this->strategies.push_back(stgy);

  	stgy.strategy = {12, 13, 14, 15}; 
  	this->strategies.push_back(stgy);

	//this->human_strategies = this->strategies;
}

void TTT4::play(bool parallelization, bool pruning, bool static_scheduling)
{
	std::cout << "\n\nDetails" << std::endl;
	if (parallelization)
	{
		std::cout << "Parallelization" << std::endl;
		std::string method = pruning? "Enabled" : "Disabled";
		std::cout << "Pruning:\t " << method << std::endl;
		method = static_scheduling? "Static" : "Dymanic";
		std::cout << "Task assignment: " << method << std::endl;
	}
	else
	{
		std::cout << "Without parallelization" << std::endl;
	}
	std::cout << "\n\n" << std::endl;
	int rand_mov = 1;
	this->print_table(  );

	for (int i = 0; i < rand_mov; ++i)
	{
		this->human_move(  );
		//this->print_strategy(  );
		this->print_table(  );
		this->computer_move(-1);
		//this->print_strategy(  );
		this->print_table(  );
	}

	while(!this->free_position.empty())
	{
		this->human_move(  );
		//this->print_strategy(  );
		this->print_table(  );
		MOVE best = this->minimax(20, this->free_position, this->strategies, (int)this->strategies.size(), pruning, static_scheduling, parallelization);
		//std::cout << "best move" << std::endl;
		this->computer_move(best.action);
		//this->print_strategy(  );
		this->print_table(  );

		//exit(-1);
	}
}

void TTT4::print_table(  )
{
	std::string space = "|";
	std::string special = " ";

	for (int it_i = 0; it_i < 4; ++it_i)
	{
		for (int it_i = 0; it_i < 24; ++it_i) std::cout << "-";
		std::cout << std::endl;

		for (int it_j = 0; it_j < 4; ++it_j)
		{
			std::cout << (it_i * 4) + it_j;
			if ((it_i * 4) + it_j < 10)
			{
				std::cout << this->table[(it_i * 4) + it_j];
			}
			std::cout << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << "|"; 
		}
		std::cout << std::endl;
		for (int it_j = 0; it_j < 4; ++it_j)std::cout << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << "|"; 
		std::cout << std::endl;
		for (int it_j = 0; it_j < 4; ++it_j)std::cout << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << this->table[(it_i * 4) + it_j] << "|"; 
		std::cout << std::endl;
	}
	for (int it_i = 0; it_i < 24; ++it_i) std::cout << "-";
	std::cout << std::endl;

}


void TTT4::human_move()
{
	int pos;
	std::cout << "\n. Turn, at position: ";
	std::cin >> pos;

	if (pos<0 || pos>15)
	{
		perror("wrong position!");
		exit(-1);
	}

	this->table[pos] = '.';
	this->increase_winner_strategy(pos, "HUMAN");

	std::vector<int>::iterator it;
  	it = std::find (this->free_position.begin(), this->free_position.end(), pos);
  	if (it == this->free_position.end())
	{
		perror("wrong position!");
		exit(-1);
	}

  	this->free_position.erase(it);
 }

 void TTT4::computer_move(int pos)
{
	std::cout << "\n* Turn, at position: ";
	int aux_pos;
	if (pos < 0)	//Select a random free position and get the position at the table
	{
		std::uniform_int_distribution<int> distribution(0,((int)this->free_position.size()) - 1);
		aux_pos = distribution(this->generator);
		pos = this->free_position[aux_pos];
	}
	else	// Receive a position at the table and look for it at the position_free vector
	{
		for (int it_p = 0; it_p < this->free_position.size(); ++it_p)
		{
			if (free_position[it_p] == pos)
			{
				aux_pos = it_p;
			}
		}
	}
	
	std::cout << pos << std::endl;
	this->table[pos] = '*';
	this->increase_winner_strategy(pos, "ME");
  	this->free_position.erase(this->free_position.begin() + aux_pos);
}

void TTT4::increase_winner_strategy(int cell, std::string strategy)
{
	int elements = (int)this->strategies.size();

	for (int it_s = 0; it_s < elements; ++it_s)
	{
		std::set<int>::iterator iter = this->strategies[it_s].strategy.find(cell);
		if (iter != this->strategies[it_s].strategy.end())
		{
			if(strategy == "ME")
			{
				this->strategies[it_s].computer_winner_cells ++;

				if (this->strategies[it_s].computer_winner_cells == (int)this->strategies[it_s].strategy.size())
				{
					std::cout << "\n\tYOU LOSE" << std::endl;
					for (std::set<int>::iterator i = this->strategies[it_s].strategy.begin(); i != this->strategies[it_s].strategy.end(); i++)
					{
						//std::cout << *i << " ";
						this->table[*i] = 'X';
					}
					this->print_table(  );
					exit(0);
				}
			}
			else if(strategy == "HUMAN")
			{
				this->strategies[it_s].human_winner_cells ++;
				if (this->strategies[it_s].human_winner_cells == (int)this->strategies[it_s].strategy.size())
				{
					std::cout << "\n\tYOU WIN" << std::endl;
					for (std::set<int>::iterator i = this->strategies[it_s].strategy.begin(); i != this->strategies[it_s].strategy.end(); i++)
					{
						//std::cout << *i << " ";
						this->table[*i] = 'X';
					}
					this->print_table(  );
					exit(0);
				}
			}
		}
	}

}
 
void TTT4::print_strategy(  )
{
	std::cout << "Strategies" << std::endl;
	for (int it_s = 0; it_s < (int)this->strategies.size(); ++it_s)
	{
	    std::set<int>::iterator it = this->strategies[it_s].strategy.begin( );

	    while(it != this->strategies[it_s].strategy.end())
	    {
	    	std::cout << *it << "\t";
	    	it ++;
	    }
	    if (this->strategies[it_s].strategy.size() < 4)
	    {
	    	std::cout << "\t";
	    }
	    std::cout << "| c: "<< this->strategies[it_s].computer_winner_cells << "\th: " << this->strategies[it_s].human_winner_cells  << std::endl; 
	}
}

// return best move
MOVE TTT4::minimax(int points, 
					std::vector< int > free_position, 
					std::vector< W_STRATEGY > strategies, 
					int t_strategies, 
					bool pruning,
					bool static_scheduling,
					bool parallelization)
{
	MOVE  mv;
	if (parallelization)
	{
		if (static_scheduling)
		{
			mv = parallel_section_static(points, free_position, strategies, t_strategies, pruning);
		}
		else
		{
			std::cout << "dymanic" << std::endl;
			mv = parallel_section_dynamic(points, free_position, strategies, t_strategies, pruning);
		}
	}
	else
	{
		mv = lineal_section(points, free_position, strategies, t_strategies);
	}
	

	//std::cout << "\n\tNEXT move: " << "points: " << mv.points << "\t|\t" << "position: " << mv.action << std::endl; 

	return mv;
}

// return the point for a strategy
MOVE parallel_section_static(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies, bool pruning)
{
	//int repetitions = 1;
	int elements = (int)free_position.size();
	MOVE max_mv = {-100, -1};
	MOVE search;
	std::vector<int> changes;

	int childs = elements > 10? 10 : elements;
	int bd_lower = 0;
	int activities = elements;
	int task;
	struct timeval tim;

	/************************************************************/
	/************************************************************/
	std::default_random_engine generator;
	generator.seed(time(NULL));
	std::uniform_int_distribution<int> distribution_key(11000,50000);
	// Threads
	pid_t my_pid, parent_pid, child_pid;

	// Semaphore
    int semnum = 4;
	std::vector<int> sids(semnum);
	std::vector<key_t> semkey(semnum);
	int key_rnd = distribution_key(generator);
	for (int it_sem = 0; it_sem < semnum; ++it_sem)
	{
		semkey[it_sem] = get_key( key_rnd + it_sem );
		sids[it_sem] = open_semaphore(semkey[it_sem]);
	}
	init_semaphore(sids[0], 1);	//	Tells the children when to start work
	init_semaphore(sids[1], childs);	//	Tells the parent when to start after all the children will finish their homework
	init_semaphore(sids[2], childs);	//	Tells to parent when all the children have been thrown, wait to take the start time
	init_semaphore(sids[3], 0);	//	Semaphore for shared memory

	// Initialize Shared memory
	int shmemnum = 1;
	std::vector<int> sidshmem(shmemnum);
	sidshmem[0] = create_shared_memory( 1 * sizeof(MOVE), generator);
	MOVE * ptShmem;
	ptShmem = (MOVE *) attach_shared_memory(sidshmem[0]);
	ptShmem[0] = {-100, -1};;
	/************************************************************/
	/************************************************************/
	//std::cout  << "childs : " << childs << std::endl;
	for (int it_i = 0; it_i < childs; ++it_i)
	{
		task = ((float)activities / (float)(childs - it_i)) + 0.5;
		//std::cout << bd_lower << ", " << task << std::endl;

		if((child_pid = fork()) < 0 )
        {
            perror("fork failure");
            delete_semaphore(sids);
            exit(1);
        }
        // fork() == 0 for child process 
        if(child_pid == 0)
        {
        	break;
        }

		bd_lower += task;
		activities -= task;
	}

	// Child process
	if(child_pid == 0)
    {
    	// ** Update semaphores status
    	down_semaphore(sids[2]);	// to start parent
    	wait_semaphore(sids[0]);	// Wait to get time

    	bool best_solution = false;

    	// ** Start Work
    	for (int it_t = 0; it_t < task && !best_solution; ++it_t)
    	{
    		//Get the table position to evaluate
    		int pos = free_position[bd_lower];
			free_position.erase(free_position.begin() + bd_lower);

    		for (int it_s = 0; it_s < t_strategies; ++it_s)	// Increase feasible cells in a strategy
			{
				std::set<int>::iterator iter = strategies[it_s].strategy.find(pos); // Current strategy uses "cell" to win?
				if (iter != strategies[it_s].strategy.end())	// If the cell could use to build a winner strategy
				{
					strategies[it_s].computer_winner_cells ++;	// Increase the number of cell that build the winner strategy
					changes.push_back(it_s);
					//give more points to get a winner solution and finish the game
					if (strategies[it_s].computer_winner_cells == strategies[it_s].strategy.size())	// A winner strategy
					{
						max_mv = {points+1, pos};
						best_solution = true;
						break;
					}
					// take a solution that prevents the opponent from winning
					else if ((strategies[it_s].human_winner_cells + 1) == strategies[it_s].strategy.size())
					{
						max_mv = {points, pos};
						best_solution = true;
						break;
					}
				}
			}

			if (!best_solution)	// There are no a best solution and looking for it
			{
				if ((int)free_position.empty()) search = {0, pos}; // No winning strategy was found and the list of moves is empty
				else search = min_value(points-1, free_position, &strategies[0], t_strategies, max_mv.points, pruning);	// minimax with free positions and winner strategies

				search.action = pos;
				if (search.points > max_mv.points) max_mv = search;	// Keep the strategy with higher points
			}

			while(!changes.empty())	// backing to the previous state the strategy table
			{
				strategies[changes.front()].computer_winner_cells --;
				changes.erase(changes.begin());
			}

    		free_position.push_back(pos);
    	}
    	// ** End work

    	// ** Update semaphores status
    	wait_semaphore(sids[3]);
		up_semaphore(sids[3]);
		if(max_mv.points > ptShmem[0].points) ptShmem[0] = max_mv;
		down_semaphore(sids[3]);
    	down_semaphore(sids[1]);	// Finish work

    	exit(-1);
    }
    else
    {
    	wait_semaphore(sids[2]);
    	// Check starting time
    	gettimeofday(&tim, NULL);
    	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
    	down_semaphore(sids[0]);
    	wait_semaphore(sids[1]);
    	// Check ending time
    	gettimeofday(&tim, NULL);
    	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
    	std::cout << (t2-t1) << " seconds elapsed" << std::endl;
    }

    max_mv = ptShmem[0];
    delete_semaphore(sids);
	delete_shared_memory(sidshmem);
	return max_mv;
}

/*

*/
// return the point for a strategy
MOVE parallel_section_dynamic(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies, bool pruning)
{
	int elements = (int)free_position.size();
	MOVE max_mv = {-100, -1};
	MOVE search;
	std::vector<int> changes;

	int childs = elements > 10? 10 : elements;
	int bd_lower = 0;
	int activities = elements;
	int task;
	struct timeval tim;

	/************************************************************/
	/************************************************************/
	std::default_random_engine generator;
	generator.seed(time(NULL));
	std::uniform_int_distribution<int> distribution_key(11000,50000);
	// Threads
	pid_t my_pid, parent_pid, child_pid;

	// Semaphore
    int semnum = 5;
	std::vector<int> sids(semnum);
	std::vector<key_t> semkey(semnum);
	int key_rnd = distribution_key(generator);
	for (int it_sem = 0; it_sem < semnum; ++it_sem)
	{
		semkey[it_sem] = get_key( key_rnd + it_sem );
		sids[it_sem] = open_semaphore(semkey[it_sem]);
	}
	init_semaphore(sids[0], 1);	//	Tells the children when to start work
	init_semaphore(sids[1], childs);	//	Tells the parent when to start after all the children will finish their homework
	init_semaphore(sids[2], childs);	//	Tells to parent when all the children have been thrown, wait to take the start time
	init_semaphore(sids[3], 0);	//	Semaphore for shared memory -> Get the BEST MOVE
	init_semaphore(sids[4], 0);	//	Semaphore for shared memory -> Branch to explore

	// Initialize Shared memory
	int shmemnum = 2;
	std::vector<int> sidshmem(shmemnum);
	sidshmem[0] = create_shared_memory( 1 * sizeof(MOVE), generator);
	sidshmem[1] = create_shared_memory( 1 * sizeof(int), generator);
	MOVE * ptShmem;
	ptShmem = (MOVE *) attach_shared_memory(sidshmem[0]);
	ptShmem[0] = {-100, -1};
	int * ptShmemPtr;
	ptShmemPtr = (int *) attach_shared_memory(sidshmem[1]);
	ptShmemPtr[0] = 0;
	/************************************************************/
	/************************************************************/
	//std::cout  << "childs : " << childs << std::endl;
	//task = activities;
	for (int it_i = 0; it_i < childs; ++it_i)
	{
		if((child_pid = fork()) < 0 )
        {
            perror("fork failure");
            delete_semaphore(sids);
            exit(1);
        }
        // fork() == 0 for child process 
        if(child_pid == 0)
        {
        	break;
        }
	}

	// Child process
	if(child_pid == 0)
    {
    	int repetitions = 1;
    	// ** Update semaphores status
    	down_semaphore(sids[2]);	// to start parent
    	wait_semaphore(sids[0]);	// Wait to get time

    	bool best_solution = false;

    	while(1)
    	{
	    	wait_semaphore(sids[4]);
			up_semaphore(sids[4]);
			std::cout << "Soy " << getpid() << " tomo recurso" << std::endl; 
			int task = ptShmemPtr[0];
			std::cout << getpid() << "\t"<< task << " de " << activities << std::endl; 
			if (task >= activities)
			{
				down_semaphore(sids[4]);
				break;
			}
			repetitions = (task == 0)? 1 : 1;
			ptShmemPtr[0] ++;
			down_semaphore(sids[4]);

			int pos = free_position[task];
			std::cout << getpid() << " pos: " << pos << "\t task: " << task << std::endl;
			//for (int i = 0; i < (int)free_position.size(); ++i) std::cout << getpid() << ":" << free_position[i] << "\t" ;
			//std::cout << std::endl;
			free_position.erase(free_position.begin() + task);

			// * Work is the same for all sections
			for (int it_s = 0; it_s < t_strategies; ++it_s)	// Increase feasible cells in a strategy
			{
				std::set<int>::iterator iter = strategies[it_s].strategy.find(pos); // Current strategy uses "cell" to win?
				if (iter != strategies[it_s].strategy.end())	// If the cell could use to build a winner strategy
				{
					strategies[it_s].computer_winner_cells ++;	// Increase the number of cell that build the winner strategy
					changes.push_back(it_s);
					//give more points to get a winner solution and finish the game
					if (strategies[it_s].computer_winner_cells == strategies[it_s].strategy.size())	// A winner strategy
					{
						max_mv = {points+1, pos};
						best_solution = true;
						break;
					}
					// take a solution that prevents the opponent from winning
					else if ((strategies[it_s].human_winner_cells + 1) == strategies[it_s].strategy.size())
					{
						max_mv = {points, pos};
						best_solution = true;
						break;
					}
				}
			}

			if (!best_solution)	// There are no a best solution and looking for it
			{
				std::cout << "\t\t\t" << getpid() << " repetitions: " << repetitions << std::endl;
				if ((int)free_position.empty()) search = {0, pos}; // No winning strategy was found and the list of moves is empty
				else
				{
					for (int it_rep = 0; it_rep < repetitions; ++it_rep)
					{
						search = min_value(points-1, free_position, &strategies[0], t_strategies, max_mv.points, pruning);	// minimax with free positions and winner strategies
					}
				}

				search.action = pos;
				if (search.points > max_mv.points) max_mv = search;	// Keep the strategy with higher points
			}

			while(!changes.empty())	// backing to the previous state the strategy table
			{
				strategies[changes.front()].computer_winner_cells --;
				changes.erase(changes.begin());
			}
			// * Work is the same for all sections

			//for (int i = 0; i < (int)free_position.size(); ++i) std::cout << getpid() << ":" << free_position[i] << "\t" ;
			//std::cout << std::endl;
			free_position.insert(free_position.begin() + task, pos);
			//for (int i = 0; i < (int)free_position.size(); ++i) std::cout << getpid() << ":" << free_position[i] << "\t" ;
			//std::cout << std::endl;
			std::cout << "estoy libre" << std::endl;
			if (best_solution) break;
		}
		// ** End work

		std::cout << "\t\t" << getpid() << " termine mi trabajo" << std::endl;
    	// ** Update semaphores status
    	wait_semaphore(sids[3]);
		up_semaphore(sids[3]);
		if(max_mv.points > ptShmem[0].points) ptShmem[0] = max_mv;
		down_semaphore(sids[3]);
    	down_semaphore(sids[1]);	// Finish work

    	exit(-1);
    }
    else
    {
    	wait_semaphore(sids[2]);
    	// Check starting time
    	gettimeofday(&tim, NULL);
    	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
    	down_semaphore(sids[0]);
    	wait_semaphore(sids[1]);
    	// Check ending time
    	gettimeofday(&tim, NULL);
    	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
    	std::cout << (t2-t1) << " seconds elapsed" << std::endl;
    }

    max_mv = ptShmem[0];
    delete_semaphore(sids);
	delete_shared_memory(sidshmem);
	return max_mv;
}



// return the point for a strategy
MOVE lineal_section(int points, std::vector< int > free_position, std::vector< W_STRATEGY >strategies, int t_strategies)
{
	int elements = (int)free_position.size();

	MOVE max_mv = {-100, -1};
	MOVE search;
	std::vector<int> changes;
	
	struct timeval tim;
	gettimeofday(&tim, NULL);
	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

	for (int it_i = 0; it_i < elements; ++it_i)
	{
		int pos = free_position.front();
		free_position.erase(free_position.begin());

		for (int it_s = 0; it_s < t_strategies; ++it_s)	// Increase feasible cells in a strategy
		{
			std::set<int>::iterator iter = strategies[it_s].strategy.find(pos); // Current strategy uses "cell" to win?
			if (iter != strategies[it_s].strategy.end())	// If the cell could use to build a winner strategy
			{
				strategies[it_s].computer_winner_cells ++;	// Increase the number of cell that build the winner strategy
				changes.push_back(it_s);
				if (strategies[it_s].computer_winner_cells == (int)strategies[it_s].strategy.size() ||
					(strategies[it_s].human_winner_cells + 1) == (int)strategies[it_s].strategy.size())	// A winner strategy
				{
					//std::cout << "\t\t\t\tWINNER!\t\t" << "points: " << points << "\tpos: " << pos << std::endl;
					while(!changes.empty())	// backing to the previous state the strategy table
					{
						strategies[changes.front()].computer_winner_cells --;
						changes.erase(changes.begin());
					}

					if(strategies[it_s].computer_winner_cells == (int)strategies[it_s].strategy.size()) return {points+1, pos};
					if(strategies[it_s].human_winner_cells + 1 == (int)strategies[it_s].strategy.size()) return {points, pos};
				}
			}
		}

		if ((int)free_position.empty()) search = {0, pos}; // No winning strategy was found and the list of moves is empty
		else search = min_value(points-1, free_position, &strategies[0], t_strategies, max_mv.points, false);	// minimax with free positions and winner strategies

		search.action = pos;
		if (search.points > max_mv.points) max_mv = search;	// Keep the strategy with higher points

		free_position.push_back(pos);	// Add the "pos" to explore other strategies

		while(!changes.empty())	// backing to the previous state the strategy table
		{
			strategies[changes.front()].computer_winner_cells --;
			changes.erase(changes.begin());
		}
	}
	
	gettimeofday(&tim, NULL);
	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
	std::cout << (t2-t1) << " seconds elapsed" << std::endl;  
	
	return max_mv;
}


// return the point for a strategy
MOVE max_value(int points, std::vector< int > free_position, W_STRATEGY *strategies, int t_strategies, int best_points, bool pruning)
{
	//std::cout << "repetitions: " << repetitions << std::endl;
	MOVE max_mv = {-100, -1};
	//std::vector< int >  keeper = free_position;
	//for (int it_repetitions = 0; it_repetitions < repetitions; ++it_repetitions)
	//{
		int elements = (int)free_position.size();

		max_mv = {-100, -1};
		MOVE search;
		std::vector<int> changes;

		for (int it_i = 0; it_i < elements; ++it_i)
		{
			int pos = free_position.front();
			free_position.erase(free_position.begin());

			for (int it_s = 0; it_s < t_strategies; ++it_s)	// Increase feasible cells in a strategy
			{
				std::set<int>::iterator iter = strategies[it_s].strategy.find(pos); // Current strategy uses "cell" to win?
				if (iter != strategies[it_s].strategy.end())	// If the cell could use to build a winner strategy
				{
					strategies[it_s].computer_winner_cells ++;	// Increase the number of cell that build the winner strategy
					changes.push_back(it_s);
					if (strategies[it_s].computer_winner_cells == strategies[it_s].strategy.size())	// A winner strategy
					{
						//std::cout << "\t\t\t\tWINNER!\t\t" << "points: " << points << "\tpos: " << pos << std::endl;
						while(!changes.empty())	// backing to the previous state the strategy table
						{
							strategies[changes.front()].computer_winner_cells --;
							changes.erase(changes.begin());
						}

						//MOVE mv = {points, pos};
						return  {points, pos};
					}
				}
			}

			if (pruning && points-1 <= best_points) search = {0, pos}; // That solution has a largest path that other solution
			if ((int)free_position.empty()) search = {0, pos}; // No winning strategy was found and the list of moves is empty
			else search = min_value(points-1, free_position, strategies, t_strategies, best_points, pruning);	// minimax with free positions and winner strategies

			search.action = pos;
			if (search.points > max_mv.points) max_mv = search;	// Keep the strategy with higher points
			free_position.push_back(pos);	// Add the "pos" to explore other strategies

			while(!changes.empty())	// backing to the previous state the strategy table
			{
				strategies[changes.front()].computer_winner_cells --;
				changes.erase(changes.begin());
			}
		}
		//free_position = keeper;
	//}
	return max_mv;
}

// return the point for a strategy
MOVE min_value(int points, std::vector< int > free_position, W_STRATEGY *strategies, int t_strategies, int best_points, bool pruning)
{
	MOVE min_mv = {100, -1};
	//std::vector< int >  keeper = free_position;

	//for (int it_repetitions = 0; it_repetitions < repetitions; ++it_repetitions)
	//{
		int elements = (int)free_position.size();

		MOVE mv = {-1, 0};
		min_mv = {100, -1};
		MOVE search;
		std::vector<int> changes;

		for (int it_i = 0; it_i < elements; ++it_i)
		{
			int pos = free_position.front();
			free_position.erase(free_position.begin());

			for (int it_s = 0; it_s < t_strategies; ++it_s)	// Increase feasible cells in a strategy
			{
				std::set<int>::iterator iter = strategies[it_s].strategy.find(pos); // Current strategy uses "cell" to win?
				if (iter != strategies[it_s].strategy.end())	// If the cell could use to build a winner strategy
				{
					strategies[it_s].human_winner_cells ++;	// Increase the number of cell that build the winner strategy
					changes.push_back(it_s);
					if (strategies[it_s].human_winner_cells == strategies[it_s].strategy.size())	// A winner strategy
					{
						//std::cout << "\t\t\t\tWINNER!\t\t" << "points: " << points << "\tpos: " << pos << std::endl;
						while(!changes.empty())	// backing to the previous state the strategy table
						{
							strategies[changes.front()].human_winner_cells --;
							changes.erase(changes.begin());
						}

						//MOVE mv = {points, pos};
						return  {points, pos};
					}
				}
			}

			if (pruning && points-1 <= best_points) search = {0, pos}; // That solution has a largest path that other solution
			if ((int)free_position.empty()) search = {0, pos}; // No winning strategy was found and the list of moves is empty
			else search = max_value(points-1, free_position, strategies, t_strategies, best_points, pruning);

			search.action = pos;
			if (search.points < min_mv.points) min_mv = search;	// Keep the strategy with higher points
			
			free_position.push_back(pos);	// Add the "pos" to explore other strategies

			while(!changes.empty())	// backing to the previous state the strategy table
			{
				strategies[changes.front()].human_winner_cells --;
				changes.erase(changes.begin());
			}
		}
		//free_position = keeper;
	//}
	return min_mv;
}
