/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "semaphore.h"

//// Standard includes. /////////////////////////////////////////////////////

//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////
key_t get_key( int val )
{
    key_t semkey = ftok(".", val);
    if ( semkey == (key_t)-1 )
      {
        perror("main: ftok() for sem failed");
        exit(-1);
      }

    return semkey;
}

int open_semaphore( key_t keyval)
{
        int     sid;
        int semnum = 1;

        if ( ! semnum )
                return(-1);
        
        if((sid = semget( keyval, semnum,  0666 | IPC_CREAT | IPC_EXCL)) == -1)
        {
            perror("IPC error 1: semget"); 
            exit(1);
        }
        
        return sid;
}

void init_semaphore( int sid, int initval)
{
	union semun {
    		int val;
    		struct semid_ds *buf;
   		unsigned short  *array;
	} arg;
        union semun semopts;   
        int id_sem = 0; 
        int rc;

        semopts.val = initval;
        if((rc = semctl( sid, id_sem, SETVAL, semopts)) == -1)
        {
        	delete_semaphore(sid);
            perror("semctl");
            exit(1);
        }
        else
        {
        	//std::cout << "semaphore was initialized" << std::endl;
        }
}

void delete_semaphore(int sid)
{
	semctl(sid, 0, IPC_RMID, 0);
	//std::cout << "The sempahore was removed" << std::endl;
}

void delete_semaphore(std::vector<int> sids)//(int sid)
{
    for (int i = 0; i < (int)sids.size(); ++i)
    {
        semctl(sids[i], 0, IPC_RMID, 0);
        //std::cout << "The sempahore was removed" << std::endl;
    }
}

void down_semaphore(int sid)
{
    struct sembuf semlock = {0, -1, 0};
    //semlock.sem_num = 0;
    if ((semop(sid, &semlock, 1)) == -1)
    {
        delete_semaphore(sid);
        perror("semop lock");
        exit(1);
    }

    //std::cout << "Semaphore -1" << std::endl;
    // union semun arg;  
    //arg.val = 0;
    //int semValue = semctl(sid, 0, GETVAL, arg);
    //std::cout << "semValue: " << semValue << std::endl;
}

void wait_semaphore(int sid)
{
    struct sembuf semwait= {0, 0, 0};

    //std::cout << "Process is waitting" <<std::endl; 
    semwait.sem_num = 0;
    if ((semop(sid, &semwait, 1)) == -1)
    {
        delete_semaphore(sid);
        perror("semop wait");
        exit(1);
    }

    //std::cout << "Semaphore is starting to work" << std::endl;
}

void up_semaphore(int sid)
{   
    struct sembuf semunlock= {0, 1, 0};

    semunlock.sem_num = 0;
    if ((semop(sid, &semunlock, 1)) == -1)
    {
        delete_semaphore(sid);
        perror("semop unlock");
        exit(1);
    }

    //std::cout << "Semaphore 1" << std::endl;
    //union semun arg;  
    //arg.val = 0;
    //int semValue = semctl(sid, 0, GETVAL, arg);
    //std::cout << "semValue: " << semValue << std::endl;
}

/*      Shared memory       */
int create_shared_memory(int size_of_shmem, std::default_random_engine generator)
{
    std::uniform_int_distribution<int> distribution_key(10000,20000);
    key_t shmkey = get_key( distribution_key(generator) ); // For shered memory

    //int sizeShmem = size_of_shmem * sizeof(double);
    /* Get the already created shared memory ID associated with key. */
    /* If the shared memory ID does not exist, then it will not be   */
    /* created, and an error will occur.                             */
    int shmid = shmget(shmkey, size_of_shmem, IPC_CREAT | 0666);
    if (shmid == -1)
    {
        perror("main: shmget() failed\n");
        exit(-1);
    }
    return shmid;
}

char * attach_shared_memory(int shmid)
{
    char *shm_address;
    /* Attach the shared memory segment to the process.       */
    shm_address = (char *)shmat(shmid, NULL, 0);
    if ( shm_address==NULL )
    {
        perror("main: shmat() failed\n");
        exit(-1);
    }

    return shm_address;
}

void delete_shared_memory(std::vector<int> sids)//(int sid)
{
    for (int i = 0; i < (int)sids.size(); ++i)
    {
        shmctl(sids[i], IPC_RMID, 0);
        //std::cout << "The shared memory was removed" << std::endl;
    }
}
